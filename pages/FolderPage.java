package com.arc.projects.pages;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.ExcelReader;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class FolderPage extends LoadableComponent<FolderPage>{
	
	
	WebDriver driver;
	private boolean isPageLoaded;	
	ProjectDashboardPage projectDashboardPage;
	PublishingPage publishingPage;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	
	@FindBy(css="#aPrjAddFolder")
	WebElement btnAddNewFolder;
	
	@FindBy(css="#txtSearchKeyword")
	WebElement txtSearchField;
	
	@FindBy(css="#btnSearch")
	WebElement btnGlobalSearch;
	
	@FindBy(css="#txtFolderName")
	WebElement txtBoxFolderName;
	
	@FindBy(css=".chkExcludedrawing")
	WebElement chkBoxExcludeLD;
	
	@FindBy(css="#btnFolderCreate")
	WebElement btnCreateFolder;
	
	@FindBy(css="#btnFolderCreateThisFolder")
	WebElement btnUploadFilesToFolder;
	
	@FindBy(css=".noty_text")
	WebElement notiMsgFolderCreate;
	
	@FindBy(css=".sel-gridview")
	WebElement handIconSelect;
	
	@FindBy(css="#docsExport")
	WebElement btnExportToCSV;
	
	@FindBy(css=".aProjHeaderLink")
	WebElement ParentFold_BreadCrumb;
	
	//Elements From File Edit window
	@FindBy(css="#txtDocName")
	WebElement txtDocName;
	
	@FindBy(css="#txtRevDate")
	WebElement txtRevisionDate;
	
	@FindBy(css="#btnEditDocAttr")
	WebElement btnSaveEditDocWindow;
	
	@FindBy(css=".btn.btn-default.btnCancel")
	WebElement btnCancelEditDocWindow;
	
	//Elements From Edit attributes window
	
	@FindBy(css="#txtImageName")
	WebElement txtPhotoName;
	
	@FindBy(xpath="//button[@class='btn btn-default btnCancel']")
	WebElement butCancelEditAttriWind;
	
	//Elements for upload and publish
	
	@FindBy(css="#aPrjUpldFile2")
	WebElement btnUploadfile;
	
	@FindBy(css="#btnKloudless")
	WebElement btnCloudAccount;
	
	@FindBy(xpath="//input[@name='qqfile']")
	WebElement btnChooseFile;
	
	@FindBy(css="#DonotIndexFiles")
	WebElement btnUploadWithoutIndex;
	
	@FindBy(css="#btnAutoOCR")
	WebElement btnUploadWithIndex;
	
	@FindBy(linkText="Process completed")
	WebElement linkProcessCompleted;
	
	@FindBy(css=".col-md-2.col-lg-2.pull-right.text-right.pblsh-padd.admrg>a")
	WebElement PublishNowwithIndex;
	
	
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public FolderPage(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	
	
	/** 
	 * Method written for Add a new folder
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean New_Folder_Create(String Foldername)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnAddNewFolder.click();//Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 20);
		SkySiteUtils.waitTill(2000);
		txtBoxFolderName.sendKeys(Foldername);//Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box." );
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");//.noty_text
		SkySiteUtils.waitForElement(driver, notiMsgFolderCreate, 20);
		String Msg_Folder_Create = notiMsgFolderCreate.getText();
		Log.message("Notification Message after folder create is: "+Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Foldername);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Foldername.trim().contentEquals(actualFolderName.trim()))
			{  	
				Log.message("Folder is created successfully with name: "+Foldername);
				break;
			}
		}
		
		
		//Final Validation - Folder Create	
		if((Foldername.trim().contentEquals(actualFolderName.trim()))&&(Msg_Folder_Create.contentEquals("Folder created successfully")))
		{
			Log.message("Folder is created successfully with name: "+Foldername);
			return true;
		}
		else
		{
			Log.message("Folder creattion FAILED with name: "+Foldername);
			return false;
		}
			
	}
	
	/** 
	 * Method written for Select a folder
	 * Scipted By: Naresh Babu
	 */
	public void Select_Folder(String Foldername)
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Foldername);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Foldername.trim().contentEquals(actualFolderName.trim()))
			{  	
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).click();//Select a folder
				Log.message("Expected Folder is clicked successfully with name: "+Foldername);
				break;
			}
		}
			
	}
	
	
	/** 
	 * Method written for Select Gallery folder
	 * Scipted By: Naresh Babu
	 */
	public void Select_Gallery_Folder()
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
	    //Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Act Name:"+actualFolderName);
	
		    //Validating the new Folder is created or not
			if(actualFolderName.trim().contentEquals("Gallery"))
			{  	
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).click();//Select Gallery folder
				Log.message("Clicked on Gallery Folder");
				break;
			}
		}
			
	}
	
	/** 
	 * Method written for Search a folder and export
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean SearchA_Folder_AndExport(String Foldername) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		txtSearchField.sendKeys(Foldername);
		Log.message("Entered Folder Name: "+Foldername+" in search edit field.");
		btnGlobalSearch.click();
		Log.message("Clicked on Search button!!!");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, handIconSelect, 20);
		//Validate the search results
		String Search_Results = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		if((Search_Results.trim()).contentEquals(Foldername))
		{
			Log.message("Search results are proper with key of: "+Foldername);
		}

			//Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
			Log.message("Download Path is: "+Sys_Download_Path);
			
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(10000);
			
			btnExportToCSV.click();
			Log.message("Clicked on Export to csv button!!!");
			SkySiteUtils.waitTill(10000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
			}
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).click();
			Log.message("Search Folder is selected successfully!!!");
			SkySiteUtils.waitTill(10000);
			
			//Get The Count of available files in search folder
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Available Folder Count is: "+Avl_File_Count);
			
			//Capturing All required validations from application
			String SubFolder_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
			String Exp_Document_Path = Foldername+" "+">>"+" "+SubFolder_Name;
			//Log.message("Exp Document Path is: "+Exp_Document_Path);
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).click();
			Log.message("Clicked on Sub folder to select.");
			SkySiteUtils.waitTill(7000);
			
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")).click();
			Log.message("Clicked on More options icon for the 1st file.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[2]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
			Log.message("Clicked on Edit Tab for the 1st file.");
			SkySiteUtils.waitForElement(driver, btnSaveEditDocWindow, 30);//Wait for save button of edit fields window
			String Exp_Document_Name = txtDocName.getAttribute("value");
			//Log.message("Exp Document Name is: "+Exp_Document_Name);
			String Exp_Revision_Date = txtRevisionDate.getAttribute("value");
			//Log.message("Exp_Revision_Date is: "+Exp_Revision_Date);
			String TimeSplitBy = " ";
			String[] Divide_Date = Exp_Revision_Date.split(TimeSplitBy);
			String Exp_Only_Date = Divide_Date[0];
			String Exp_Only_Time = Divide_Date[1];
			String Exp_Only_Noon = Divide_Date[2];
			//Log.message("Exp_OnlyRevision_Date is: "+Exp_Only_Date);
			//Log.message("Exp_OnlyRevision_Time is: "+Exp_Only_Time);
			//Log.message("Exp_OnlyRevision_AMorPM is: "+Exp_Only_Noon);
			
			btnCancelEditDocWindow.click();//Close the edit window
			
			//Validating the CSV file from download folder	
			String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
			BufferedReader br = null;
			String line = null; 
			String splitBy = ",";
			int count = 0;
			String ActFileName = null;
			String ActReviDate = null;
			String ActRevNumber = null;
			String ActDocuPath = null;
			String ActPublishStatus = null;
			int Match_Count = 0;
			
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				//Log.message("Downloaded csv file have data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				ActFileName = ActValue[0];
				ActFileName = ActFileName.replace("\"", "");
				//Log.message("Document Name from csv file is: "+ ActFileName);
				ActReviDate = ActValue[1];
				ActReviDate = ActReviDate.replace("\"", "");
				//Log.message("Document Revision Date from csv file is: "+ ActReviDate);
				String[] Divide_Date_csv = ActReviDate.split(TimeSplitBy);
				String Act_Only_Date = Divide_Date_csv[0];
				String Act_Only_Time = Divide_Date_csv[1];
				//String Act_Only_Noon = Divide_Date_csv[2];
				//Log.message("Act_OnlyRevision_Date is: "+Act_Only_Date);
				//Log.message("Act_OnlyRevision_Time is: "+Act_Only_Time);
				//Log.message("Act_OnlyRevision_AMorPM is: "+Act_Only_Noon);
				ActRevNumber = ActValue[2];
				ActRevNumber = ActRevNumber.replace("\"", "");	
				//Log.message("Document Revision Number from csv file is: "+ ActRevNumber);
				ActDocuPath = ActValue[3];
				ActDocuPath = ActDocuPath.replace("\"", "");
				//Log.message("Document Path from csv file is: "+ ActDocuPath);
				ActPublishStatus = ActValue[7];
				ActPublishStatus = ActPublishStatus.replace("\"", "");
				//Log.message("Is document published or nor from csv file is: "+ ActPublishStatus);
				 
				if((ActFileName.contains(Exp_Document_Name))&&(ActRevNumber.contentEquals("1"))&&(ActPublishStatus.equalsIgnoreCase("Yes"))
						&&(Act_Only_Date.contentEquals(Exp_Only_Date))&&(Act_Only_Time.contains(Exp_Only_Time))
						&&(ActReviDate.contains(Exp_Only_Noon))&&(ActDocuPath.contentEquals(Exp_Document_Path)))
				{
					Match_Count = Match_Count+1;
				}
			}
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			Avl_File_Count=Avl_File_Count+3;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Search Folder Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Search Folder Exported File is NOT having data properly!!!");
			return false;
		}
			
	}

	
	/** 
	 * Method written for Search a Sub folder(5th level) and export
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean SearchA_SubFolder_AndExport(String Foldername) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		txtSearchField.sendKeys(Foldername);
		Log.message("Entered Folder Name: "+Foldername+" in search edit field.");
		btnGlobalSearch.click();
		Log.message("Clicked on Search button!!!");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, handIconSelect, 20);
		//Validate the search results
		String Search_Results = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		if((Search_Results.trim()).contentEquals(Foldername))
		{
			Log.message("Search results are proper with key of: "+Foldername);
		}

			//Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
			Log.message("Download Path is: "+Sys_Download_Path);
			
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(10000);
			
			btnExportToCSV.click();
			Log.message("Clicked on Export to csv button!!!");
			SkySiteUtils.waitTill(10000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
			}
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).click();
			Log.message("Search Folder is selected successfully!!!");
			SkySiteUtils.waitTill(5000);
			
			//Get The Count of available files in selected folder
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Available File Count is: "+Avl_File_Count);
			
			//Capturing All required validations from application
			String SubFolder_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[4]/span/a")).getText();
			String SubFolder1_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[5]/span/a")).getText();
			String SubFolder2_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[6]/span/a")).getText();
			String SubFolder3_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[7]/span/a")).getText();
			String SubFolder4_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[8]/span/a")).getText();

String Exp_Document_Path = SubFolder_Name+" >> "+SubFolder1_Name+" >> "+SubFolder2_Name+" >> "+SubFolder3_Name+" >> "+SubFolder4_Name+" >> "+Foldername;
			//Log.message("Exp Document Path is: "+Exp_Document_Path);
			
			String Exp_Document_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
			//Log.message("Exp Document Name is: "+Exp_Document_Name);
			
			//Validating the CSV file from download folder	
				String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
				BufferedReader br = null;
				String line = null; 
				String splitBy = ",";
				int count = 0;
				String ActFileName = null;
				String ActRevNumber = null;
				String ActDocuPath = null;
				String ActPublishStatus = null;
				int Match_Count = 0;
				
				br = new BufferedReader(new FileReader(csvFileToRead)); 
				while ((line = br.readLine()) != null) 
				{  
					count=count+1;
					//Log.message("Downloaded csv file have data in : "+count+ " rows");
					String[] ActValue = line.split(splitBy);
					ActFileName = ActValue[0];
					ActFileName = ActFileName.replace("\"", "");
					//Log.message("Document Name from csv file is: "+ ActFileName);				
					ActRevNumber = ActValue[2];
					ActRevNumber = ActRevNumber.replace("\"", "");	
					//Log.message("Document Revision Number from csv file is: "+ ActRevNumber);
					ActDocuPath = ActValue[3];
					ActDocuPath = ActDocuPath.replace("\"", "");	
					//Log.message("Document Path from csv file is: "+ ActDocuPath);
					ActPublishStatus = ActValue[7];
					ActPublishStatus = ActPublishStatus.replace("\"", "");
					//Log.message("Is document published or not from csv file is: "+ ActPublishStatus);
					 
					if((ActFileName.contains(Exp_Document_Name))&&(ActRevNumber.contentEquals("1"))&&(ActPublishStatus.equalsIgnoreCase("Yes"))
							&&(ActDocuPath.contentEquals(Exp_Document_Path)))
					{
						Match_Count = Match_Count+1;
					}
				}
				Log.message("Downloaded csv file have data in : "+count+ " rows");
				Avl_File_Count=Avl_File_Count+1;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Search Sub Folder Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Search Sub Folder Exported File is NOT having data properly!!!");
			return false;
		}
			
	}

	
	
	/** 
	 * Method written for export a folder contains unsupported files (.jpg, excel) 
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Export_Folder_WithUnSupportFiles() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		
	//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
			
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
			
		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);
			
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
			
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
				
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}

		String Foldername = PropertyReader.getProperty("Fold_UnSuportFiles");
		this.Select_Folder(Foldername);//Calling Select expected Folder
		SkySiteUtils.waitTill(5000);
			
		//Get The Count of available files in selected folder
		int Avl_File_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_File_Count = Avl_File_Count+1; 	
		} 
		Log.message("Available File Count is: "+Avl_File_Count);
			
		//Capturing All required validations from application
		String Exp_Document_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		Log.message("Exp Document Name is: "+Exp_Document_Name);
		
		//Validating the CSV file from download folder	
			String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
			BufferedReader br = null;
			String line = null; 
			String splitBy = ",";
			int count = 0;
			String ActFileName = null;
			String ActRevNumber = null;
			String ActDocuPath = null;
			String ActPublishStatus = null;
			int Match_Count = 0;
			
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				//Log.message("Downloaded csv file have data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				ActFileName = ActValue[0];
				ActFileName = ActFileName.replace("\"", "");
				Log.message("Document Name from csv file is: "+ ActFileName);				
				ActRevNumber = ActValue[2];
				ActRevNumber = ActRevNumber.replace("\"", "");	
				Log.message("Document Revision Number from csv file is: "+ ActRevNumber);
				ActDocuPath = ActValue[3];
				ActDocuPath = ActDocuPath.replace("\"", "");	
				Log.message("Document Path from csv file is: "+ ActDocuPath);
				ActPublishStatus = ActValue[7];
				ActPublishStatus = ActPublishStatus.replace("\"", "");
				Log.message("Is document published or nor from csv file is: "+ ActPublishStatus);
				 
				if((ActFileName.contains(Exp_Document_Name))&&(ActRevNumber.contentEquals("1"))&&(ActPublishStatus.equalsIgnoreCase("Yes"))
						&&(ActDocuPath.contentEquals(Foldername)))
				{
					Match_Count = Match_Count+1;
				}
			}
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			Avl_File_Count=Avl_File_Count+1;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Folder having unsupported files Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Folder having unsupported files Exported File is NOT having data properly!!!");
			return false;
		}
			
	}
	
	
	/** 
	 * Method written for Gallery folder export
	 *  Scipted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Gallery_Folder_Export() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		
	//Calling "Select Gallery Folder" Method
		this.Select_Gallery_Folder();
		SkySiteUtils.waitTill(5000);
		
	//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
			
		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);
			
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
			
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
				
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
			
		//Get The Count of available files in search folder
		int Avl_File_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'photo-viewer preview')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_File_Count = Avl_File_Count+1; 	
		} 
		Log.message("Available Items Count in Gallery is: "+Avl_File_Count);
			
			//Capturing All required validations from application
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[5]")).click();
			Log.message("Clicked on More options icon for the 1st Photo.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[6]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
			Log.message("Clicked on Edit Tab for the 1st Photo.");
			SkySiteUtils.waitForElement(driver, txtPhotoName, 30);//Wait for save button of edit fields window
			SkySiteUtils.waitTill(2000);
			String Exp_Photo_Name = txtPhotoName.getAttribute("value");
			//Log.message("Exp Photo Name is: "+Exp_Photo_Name);
			String Exp_Building = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[1]")).getAttribute("value");
			//Log.message("Exp Building Name is: "+Exp_Building);
			String Exp_Level = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[2]")).getAttribute("value");
			//Log.message("Exp Level Name is: "+Exp_Level);
			String Exp_Room = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[3]")).getAttribute("value");
			//Log.message("Exp room Name is: "+Exp_Room);
			String Exp_Area = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[4]")).getAttribute("value");
			//Log.message("Exp Area is: "+Exp_Area);
			String Exp_Description = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[5]")).getAttribute("value");
			//Log.message("Exp Description is: "+Exp_Description);
			
			butCancelEditAttriWind.click();//Close the edit attribute window
			
			//Validating the CSV file from download folder	
			String csvFileToRead = PropertyReader.getProperty("Gallery_Exp_Path");
			BufferedReader br = null;
			String line = null; 
			String splitBy = ",";
			int count = 0;
			String ActPhotoName = null;
			String ActBuilding = null;
			String ActBuildLevel = null;
			String ActRoom = null;
			String ActArea = null;
			String ActDescription = null;
			int Match_Count = 0;
			
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				//Log.message("Downloaded csv file have data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				ActPhotoName = ActValue[0];
				//Log.message("Photo Name from csv file is: "+ ActPhotoName);			
				ActBuilding = ActValue[4];
				//Log.message("Building Name from csv file is: "+ ActBuilding);				
				ActBuildLevel = ActValue[5];	
				//Log.message("Building Level from csv file is: "+ ActBuildLevel);				
				ActRoom = ActValue[6];
				//Log.message("Room Number From csv file is: "+ ActRoom);				
				ActArea = ActValue[7];
				//Log.message("Room Area From csv file is: "+ ActArea);				
				ActDescription = ActValue[8];
				//Log.message("Description of Building from csv file is: "+ ActDescription);
				 
				if((ActPhotoName.contains(Exp_Photo_Name))&&(ActBuilding.contentEquals(Exp_Building))&&(ActBuildLevel.contains(Exp_Level))
						&&(ActRoom.contains(Exp_Room))&&(ActArea.contentEquals(Exp_Area))&&(ActDescription.contentEquals(Exp_Description)))
				{
					Match_Count = Match_Count+1;
				}
			}
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			Avl_File_Count=Avl_File_Count+2;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Gallery Folder Exported CSV File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Gallery Folder Exported CSV File is NOT having data properly!!!");
			return false;
		}
			
	}
	
	
	/** 
	 * Method written for Validate uploaded files from folder
	 *  Scipted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Validate Files are uploaded properly or not
	public boolean ValidateUploadFiles(String FolderPath,int FileCount) throws InterruptedException
	{	
		boolean result = false;
		try
		{	
			String ActFileName=null;
			String expFilename=null;
			int uploadSuccessCount=0;
			//Reading all the file names from input folder
			File[] files = new File(FolderPath).listFiles();
			
			for(File file : files)
			{
				if(file.isFile()) 
				{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				SkySiteUtils.waitTill(500);
				for(int n=2;n<=FileCount+1;n++)
				{											
				ActFileName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+n+"]/section[2]/h4/span[1]")).getText(); 
				Log.message("Actual File name is:"+ActFileName);
					     
					if(expFilename.trim().equalsIgnoreCase(ActFileName.trim()))
					{	    			    	 
					  uploadSuccessCount=uploadSuccessCount+1;
					  Log.message("File uploded success for:" +ActFileName+"Sucess Count:"+uploadSuccessCount);
					  break;
					}
				}
			     		     
				}
					     
			}
					     
			//Checking whether file count is equal or not		     
			if(FileCount==uploadSuccessCount)	
			{
				Log.message("All the files are available in folder.");
				result=true;
			}
			else
			{	
				Log.message("All the files are NOT available in folder.");
				result=false;
			}
			
		}//End of try block
			
		catch(Exception e)
		{	
			result=false;	
		}
			
		finally
		{
				return result;	
		}
	}

	
	/** 
	 * Method written for upload files using without index
	 *  Scipted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean UploadFiles_WithoutIndex(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadfile.click();//Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();//Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(15000);

	//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;		
		randomFileName rn=new randomFileName();
		String tmpFileName="c:/"+rn.nextFileName()+".txt";							
		output = new BufferedWriter(new FileWriter(tmpFileName,true));					
		String expFilename=null;
		File[] files = new File(FolderPath).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);	
			}
		}

		output.flush();
		output.close();
					
	//Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoIT_File_Path");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");			
		SkySiteUtils.waitTill(10000);
		
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
	//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete()){
			Log.message(file.getName() + " is deleted!");
			}else{
				Log.message("Delete operation is failed.");
				}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();//Click on Upload without index  
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notiMsgFolderCreate, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notiMsgFolderCreate.getText();
		Log.message("Notification Message after upload is: "+UploadSuccess_Message);

		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 30);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(10000);
		
		result = this.ValidateUploadFiles(FolderPath, FileCount);//Calling validate files method
		
		if(result==true)
		{
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}		
		
	}
	
	
	/** 
	 * Method written for upload files using with index
	 *  Scipted By: Ranjan P
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public PublishingPage UploadFiles_WithIndexing(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();//Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnChooseFile.click();//Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(8000);

	//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;		
		randomFileName rn=new randomFileName();
		String tmpFileName="c:/"+rn.nextFileName()+".txt";							
		output = new BufferedWriter(new FileWriter(tmpFileName,true));					
		String expFilename=null;
		File[] files = new File(FolderPath).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:" +expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(3000);	
			}
		}

		output.flush();
		output.close();
					
	//Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoIT_File_Path");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");			
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(8000);
	//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete()){
			Log.message(file.getName() + " is deleted!");
			}else{
				Log.message("Delete operation is failed.");
				}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		
		//Click on Upload with index  
		SkySiteUtils.waitTill(10000);
		btnUploadWithIndex.click();
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, notiMsgFolderCreate, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notiMsgFolderCreate.getText();
		Log.message("Notification Message after upload is: "+UploadSuccess_Message);

		SkySiteUtils.waitForElement(driver, PublishNowwithIndex, 180);
		SkySiteUtils.waitTill(5000);
		PublishNowwithIndex.click();
		Log.message("Clicked on Publish Now link .");
		//SkySiteUtils.waitTill(15000);	
		
		return new PublishingPage(driver).get();
		
		/*result = this.ValidateUploadFiles(FolderPath, FileCount);//Calling validate files method
		
		if(result==true)
		{
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}		*/
	
	}
	
	
	
	
	
	

	
}
