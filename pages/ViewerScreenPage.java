package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ViewerScreenPage extends LoadableComponent<ViewerScreenPage>{
	
	WebDriver driver;
	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;
	private boolean isPageLoaded;
	
	
	//=====ZoomIn Button======================
	@FindBy(css=".leaflet-control-zoom-in")
	WebElement ZoomIn;
	//=====ZoomOut Button======================
	@FindBy(css=".leaflet-control-zoom-out")
	WebElement ZoomOut;
	//========HomeButton======================
	@FindBy(css=".leaflet-control-zoom-out")
	WebElement Home;
	//========Full Screen Button===============
	@FindBy(css=".leaflet-control-zoom-fullscreen.leaflet-bar-part.leaflet-bar-part-bottom.last")
	WebElement FullScreen;	
	//========Pointer Arrow===============
	@FindBy(css="#pointerToolMenuBtn")
	WebElement OpenPointer_Menu;	
	//========Pointer Arrow_Submenu_Arrow===========
	@FindBy(css="#panSelectMenuBtn")
	WebElement Pointer_Arrow_submenu;
	//========Pointer Arrow_Submenu_Multi_select====
	@FindBy(css=".icon.icon-lg.icon-multi_select")
	WebElement Pointer_Arrow_submenu_MultiSelect;	
	//========OpenLineMenu==========================
	@FindBy(xpath=".//*[@id='lineToolMenuBtn']")
	WebElement OpenLine_Menu;
	//========Drow Line_Submenu =====================
	@FindBy(xpath=".//*[@id='lineCreate']/a")
	WebElement DrowLine;
	//========Drow With FreeHand ==================
	@FindBy(xpath=".//*[@id='penCreate']/a/i")
	WebElement Drowwithfreehand;
	//========Drow An Arrow ==================
	@FindBy(css=".leaflet-draw-draw-arrow")
	WebElement Drow_an_Arrow;	
	//========Retangle High-lighter ==============
	@FindBy(css=".icon.icon-lg.icon-highlight")
	WebElement RetrangleHighlighter;	
	//========Open Shape Menu  ==================
	@FindBy(css=".icon.icon-lg.icon-shape_group")
	WebElement OpenShape_Menu;	
	//========Drow Retangle Sub Menu  ============
	@FindBy(css=".leaflet-draw-draw-rectangle")
	WebElement Draw_Retangle;
	//========Drow Eclipse  ====================
	@FindBy(css=".icon.icon-lg.icon-circle2")
	WebElement Draw_Eclipse;
	//========Drow Cloud  ====================
	@FindBy(css=".icon.icon-lg.icon-cloud2")
	WebElement Draw_Cloud;	
	//=========Open Text menu================	
	@FindBy(css="#textToolMenuBtn")
	WebElement OpenTextMenu;		
	//=========Add Text Link================	
	@FindBy(xpath=".//*[@id='textCreate']/a/i")
	WebElement Add_A_Text;	
	//=========Add Text Note================	
	@FindBy(xpath=".//*[@id='markerCreate']/a/i")
	WebElement Add_A_note;		
	//=========Add Text Callout================	
	@FindBy(xpath=".//*[@id='callOutCreate']/a")
	WebElement Add_A_Callout;	
	//=========Open_Hyperlink_Menu================	
	@FindBy(css=".icon.icon-lg.icon-hyperlink-group")
	WebElement Open_Hyperlink_menu;
			
	//=========Open_Hyperlink_Submenu Cloud========
	@FindBy(xpath=".//*[@id='linkCloudCreate']/a")
	WebElement Hyperlink_Cloud;
	//=========Open_Hyperlink_Submenu Eclipse======
	@FindBy(xpath=".//*[@id='linkEllipseCreate']/a")
	WebElement Hyperlink_Circle;
		
	//=========Open_Hyperlink_Submenu Retangle======
	@FindBy(xpath=".//*[@id='linkRectCreate']/a/i")
	WebElement Hyperlink_Retangle;
	
	//=========Create RFI=========================
	@FindBy(xpath=".icon.icon-lg.icon-rfi")
	WebElement Create_RFI;
	
	//=========Add Photo=========================
	@FindBy(xpath=".leaflet-draw-draw-photo")
	WebElement AddPhoto;		
	
	//=========Create Punch=========================
	@FindBy(xpath=".icon.icon-lg.icon-app-punch")
	WebElement CreatePunch;

	//Export Related Elements
	@FindBy(css="#aPrivateProjects")
	WebElement PrivateProjectsTab;
	
	
	
	//=========Filename Selection=========================
	@FindBy(xpath=".preview.document-preview-event")
	WebElement FileName_preview;
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, btnLogOff, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public ViewerScreenPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
		
	
	
	public boolean ToolBar_Selection_And_Validation() throws Throwable
	
	{
		boolean result = false;
		SkySiteUtils.waitTill(500);
		
		driver.switchTo().defaultContent();
		//CommonMethod.mouseover(FileName_preview);
		FileName_preview.click();


		SkySiteUtils.waitTill(3000);
		//==== ZoomIn button validation	=======
		
		ZoomIn.click();
		Log.message("Zoom In Button clicked sucessfully -First time.");
		SkySiteUtils.waitTill(3000);
		ZoomIn.click();
		Log.message("Zoom In Button clicked sucessfully -Second time.");
		SkySiteUtils.waitTill(3000);
		ZoomIn.click();
		Log.message("Zoom In Button clicked sucessfully -Third time.");
		
		//ZoomOut Button Clicked Sucessfully.
		SkySiteUtils.waitTill(3000);
		return result;
		
		
	
			
		
	}
	
	
	
	

}
