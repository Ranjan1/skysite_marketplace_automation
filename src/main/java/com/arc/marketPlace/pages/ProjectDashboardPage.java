package com.arc.marketPlace.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.marketPlace.utils.Generate_Random_Number;
import com.arc.marketPlace.utils.PropertyReader;
import com.arc.marketPlace.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;


public class ProjectDashboardPage  extends LoadableComponent<ProjectDashboardPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	
	@FindBy(css=".dropdown.sec-pref.open>a")
	WebElement btnLogOff;
	
	 //@FindBy(css="div.modal-footer .btn.btn-default")
	@FindBy(xpath=".//*[@id='WhatsNewFeature']/div/div/div[3]/button[1]")
	WebElement btnSkip;
	
	//@FindBy(css=".close")
	@FindBy(xpath=".//*[@id='WhatsNewFeature']/div/div/div[1]/button")
	WebElement btnClose;
	
	@FindBy(css=".ltmenu-user.badge-stack.dropdown>a")
	WebElement btnProfile;
	
	@FindBy(css="#txtSearchKeyword")
	WebElement txtGlobalSearch;
	@FindBy(css="#btnSearch")
	WebElement bunSearch;
	
	@FindBy(css="#add-project")
	WebElement btnCreateProject;
	
	@FindBy(css="#btnCreate")
	WebElement btnCreate;
	
	@FindBy(css="#txtProjectName")
	WebElement txtProjectName;
	
	@FindBy(css="#txtProjectNumber")
	WebElement txtProjectNumber;
	
	@FindBy(xpath=".//*[@id='prj-start-date']/span")
	WebElement butProjectStartDate;
	
	@FindBy(css="#txtProjectDescription")
	WebElement txtProjectDescription;
	
	@FindBy(css="#txtAddress")
	WebElement txtAddress;
	
	@FindBy(css="#txtProjectStartDate")
	WebElement txtProjectStartDate;
	
	@FindBy(css="#txtCity")
	WebElement txtCity;
	
	@FindBy(css="#txtState")
	WebElement txtState;
	
	@FindBy(css="#txtZip")
	WebElement txtZip;
	
	@FindBy(css="#txtCountry")
	WebElement txtCountry;
	
	@FindBy(css="#txtProjectPassword")
	WebElement txtProjectPassword;
	
	@FindBy(css=".noty_text")
	WebElement projectCreationSuccessMsg;
	
	@FindBy(css=".Country-Item[title='USA']")
	WebElement countryTitleItem;
	
	@FindBy(css=".State-Item[data-name='alabama']")
	WebElement stateTitleItem;
	
	@FindBy(css="#isAutoHyperlinkEnabled")
	WebElement checkboxEnableAutoHypLink;
	
	@FindBy(xpath="(//button[@class='btn btn-default pull-right'])[4]")
	WebElement btnSavePrjSettingsWin;
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	//Export Related Elements
	@FindBy(css="#aPrivateProjects")
	WebElement PrivateProjectsTab;
	
	@FindBy(xpath="//*[contains(@id, 'PName_')]")
	WebElement All_Proj_Name;
	
	@FindBy(css="#Export-Project")
	WebElement ProjLev_ExptoCSV_Button;
	
	@FindBy(xpath="(//i[@class='icon icon-ellipsis-horizontal icon-lg'])[1]")
	WebElement Prj_MoreOptions;
	
	@FindBy(xpath="//a[contains(text(),'Edit')])[1]")
	WebElement Edit_Tab_FirstPrj;
	
	@FindBy(css=".last")
	WebElement ProjName_Breadcrumb;
	
	//Contacts button
	@FindBy(css=".icon.icon-app-contact.icon-lg.pw-icon-white")
	WebElement btnContacts;
	
	@FindBy(css="#add-contact")
	WebElement btnAddNewContact;
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, btnLogOff, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public ProjectDashboardPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/** 
	 * Method written for checking the availability of Profile button.
	 * @return
	 */
	public boolean presenceOfProfileButton()
	{
		SkySiteUtils.waitTill(8000);
		String Parent_Window = driver.getWindowHandle(); 
		for (String Child_Window : driver.getWindowHandles())  
	     {  
	     driver.switchTo().window(Child_Window); 
	     JavascriptExecutor js = (JavascriptExecutor)driver;
	     Boolean btnCloseIsPresent = driver.findElements(By.xpath(".//*[@id='WhatsNewFeature']/div/div/div[1]/button")).size()>0;
	     if(btnCloseIsPresent.equals(true))
	     {
	     js.executeScript("arguments[0].click();", btnClose);
	     Log.message("Modal window is closed.");
	     }
	    /* else
	     {
	     SkySiteUtils.waitTill(10000);
	 	 driver.switchTo().window(Parent_Window); 
	     } */
	     }
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		Log.message("Checking whether Profile button is present?");
		if(btnProfile.isDisplayed())
			return true;
		else
			return false;
		
	}
	
	/** 
	 * Method written for creating new project.
	 * Scripted By: Naresh
	 * @return
	 */
	
	
	/** 
	 * Method written for Deleting Existed files from the download folder.
	 * Scripted By : NARESH BABU
	 * @return
	 */
	//Deleting files from a folder
	public void Delete_ExistedFiles_From_DownloadFolder(String Sys_Download_Path)throws InterruptedException 
	{	
		try 
		{ 
			SkySiteUtils.waitTill(10000);
			Log.message("Going to Clean existed files from download folder!!!");
			File file = new File(Sys_Download_Path);      
		    String[] myFiles;    
		    if(file.isDirectory())
		    {
		    	myFiles = file.list();
		        for (int i=0; i<myFiles.length; i++) 
		        {
		        	File myFile = new File(file, myFiles[i]); 
		            myFile.delete();
		            SkySiteUtils.waitTill(10000);
		        }
		        Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		    }
		           
		}//End of try block

		catch(Exception e)
		{
			Log.message("Unable to delete Available Folders/File from download folder!!!"+e);
		}
	}

	/** 
	 * Method written for validating Projects Export.
	 * Scripted By : NARESH BABU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Validate_ProjectExport(String csvFileToRead) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, ProjLev_ExptoCSV_Button, 30);
		//PrivateProjectsTab.click();
		//Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
		
		//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		//Getting Required values from app to compare with the values from downloaded csv file.
		String ExpProjName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+Avl_Projects_Count+"]/div/section[1]/h4/span")).getText();
		Log.message("Expected Project Name From APP is: "+ExpProjName);
		String ExpProjOwnerName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+Avl_Projects_Count+"]/div[1]/section[1]/div/span[1]")).getText();
		Log.message("Expected Project Owner Name From APP is: "+ExpProjOwnerName);	
		String ExpProjSize = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+Avl_Projects_Count+"]/div[1]/section[1]/div/span[2]")).getText();
		Log.message("Expected Project Size From APP is: "+ExpProjSize);
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		this.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		ProjLev_ExptoCSV_Button.click();//Click on Export to csv
		Log.message("Export to Csv has been clicked!!!");
		SkySiteUtils.waitTill(20000);
		
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
		
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
			
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		
		
	//After export projects, checking whether exported file is existed under download folder or not 
		String ActualFilename=null;
		File[] files = new File(Sys_Download_Path).listFiles();
					
		for(File file : files)
		{
			if(file.isFile()) 
			{
				ActualFilename=file.getName();//Getting File Names into a variable
				Log.message("Actual File name is:"+ActualFilename);
				SkySiteUtils.waitTill(1000);	
				if(ActualFilename.contains("Documents"))						
				{
					Log.message("Downloaded csv file is available in downloads!!!");							
					break;
				}
							
			}
						
		}
		
		//Validating the CSV file from download folder	 
		//String csvFileToRead = "C:\\Users\\nareshk\\Downloads\\Documents.csv";
		BufferedReader br = null;
		String line = null; 
		String splitBy = ",";
		int count = 0;
		String ActProjName = null;
		String ActProjOwnerName = null;
		String ActProjSize = null;
		int Match_Count = 0;
		
		br = new BufferedReader(new FileReader(csvFileToRead)); 
		while ((line = br.readLine()) != null) 
		{  
			count=count+1;
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			String[] ActValue = line.split(splitBy);
			ActProjName = ActValue[1];
			ActProjName = ActProjName.replace("\"", "");
			Log.message("Project Name from csv file is: "+ ActProjName);
			ActProjOwnerName = ActValue[8];
			ActProjOwnerName = ActProjOwnerName.replace("\"", "");
			Log.message("Project Owner Name from csv file is: "+ ActProjOwnerName);
			ActProjSize = ActValue[11];
			ActProjSize = ActProjSize.replace("\"", "");
			Log.message("Project size from csv file is: "+ ActProjSize);
				
			if((ActProjName.contentEquals(ExpProjName))
					&&(ActProjOwnerName.contentEquals(ExpProjOwnerName))&&(ActProjSize.contentEquals(ExpProjSize)))
			{
				Log.message("Projects Exported File is having data properly!!!");
				Match_Count = Match_Count+1;
			}
		}
		Log.message("Downloaded csv file have data in : "+count+ " rows");
		Avl_Projects_Count=Avl_Projects_Count+1;
		
		if((count==Avl_Projects_Count)&&(Match_Count == 1))
		{
			return true;
		}
		else
		{
			Log.message("Projects Exported File is NOT having data properly!!!");
			return false;
		}
	
	}
	
	
	/** 
	 * Method written for validating Export to csv by editing all the fields of a project.
	 * Scripted By : NARESH BABU KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	public boolean Validate_ModifiedProject_Export() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		//PrivateProjectsTab.click();
		WebElement EditLink = PrivateProjectsTab;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click projects
		Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
		
		//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		//Generating all input values randomly
				String Random_Number = Generate_Random_Number.generateRandomValue();
				String Edit_projectName = "Prj_"+Random_Number;
				String Edit_projectNumber = Random_Number;
				String Edit_Description = "Description_"+Random_Number;
				String Edit_SiteAddress = "SiteAddress_"+Random_Number;
				String Edit_City = "City_"+Random_Number;
				String Edit_state = null;
				String Edit_Country = null;
				String Edit_Zip = Random_Number;
				String Edit_Password = Random_Number;
				String Edit_Date = null;
		
		
		if(Avl_Projects_Count>=1)
		{
			//Edit 1st located project
			Prj_MoreOptions.click();//Click on 1st project More options
			SkySiteUtils.waitTill(5000);
			//SkySiteUtils.waitForElement(driver, Edit_Tab_FirstPrj, 30000);
			WebElement EditLink1 = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[1]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink1);//JSExecutor to click edit
			SkySiteUtils.waitForElement(driver, btnCreate, 30000);//Wait for update project button
			
			txtProjectName.clear();//Clear the existed project name
			txtProjectName.sendKeys(Edit_projectName);//Enter new project name
		
			txtProjectNumber.clear();//Clear the existed Project Number
			txtProjectNumber.sendKeys(Edit_projectNumber);//Enter Project Number
		
			SkySiteUtils.waitTill(2000);
			butProjectStartDate.click();//Clicking on Project Start Date symbol
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("//td[@class='day active']")).click();//Select today's date
			Log.message("Date is selected!!!");
			SkySiteUtils.waitTill(3000);
			Edit_Date = txtProjectStartDate.getAttribute("value");
			Log.message("Selected Date is: "+Edit_Date);
			
			txtProjectDescription.clear();//Clear the existed Description
			txtProjectDescription.sendKeys(Edit_Description);//Enter Project Description
		
			txtAddress.clear();//Clear the existed Site Address
			txtAddress.sendKeys(Edit_SiteAddress);//Enter Site Address
		
			txtCity.clear();//Clear Existed City
			txtCity.sendKeys(Edit_City);//Enter City
		
			//Generating a random value in between 1 to 200
			int Max = 201;
			int Min = 1;
			//Create Instance of Random Class
			Random ranomNum = new Random();
			int Gen_Random_Val = Min + ranomNum.nextInt(Max);
			Log.message("Genarated Random Value is: "+Gen_Random_Val);
			
			
			driver.findElement(By.xpath("//div[@id='ddlCountrydrop']")).click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='Country-Item'])["+Gen_Random_Val+"]")).click();//Select Any Country
			SkySiteUtils.waitTill(2000);
			Edit_Country = txtCountry.getAttribute("value");
			Log.message("Edit Country is: "+Edit_Country);
		
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//div[@id='ddlstatedrop']")).click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='State-Item'])[1]")).click();//Select Any State
			SkySiteUtils.waitTill(2000);
			Edit_state = txtState.getAttribute("value");
			Log.message("Edit State is: "+Edit_state);
		
			txtZip.clear();//Clear Existed Zip
			txtZip.sendKeys(Edit_Zip);//Enter Zip
			
			txtProjectPassword.clear();//Clear Existed Password
			txtProjectPassword.sendKeys(Edit_Password);//Enter Password
			
			btnCreate.click();//Click on Create Project button
			Log.message("Clicked on Update Project Button!!!");
			SkySiteUtils.waitTill(1000);
			
			SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 30);//Wait until message get displayed
			String MsgAfterProjEdit = projectCreationSuccessMsg.getText();//Getting Notification message
			
			Log.message("Notification message After project edit is:"+MsgAfterProjEdit);
			SkySiteUtils.waitTill(10000);
	}
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		this.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		//ProjLev_ExptoCSV_Button.click();//Click on Export to csv
		WebElement EditLink2 = ProjLev_ExptoCSV_Button;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink2);//JSExecutor to click edit
		Log.message("Export to Csv has been clicked!!!");
		SkySiteUtils.waitTill(60000);
		
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
		
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot class
			Robot robot=null;
			robot=new Robot();
			
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		
	//After export projects, checking whether exported file is existed under download folder or not 
		String ActualFilename=null;
		File[] files = new File(Sys_Download_Path).listFiles();
					
		for(File file : files)
		{
			if(file.isFile()) 
			{
				ActualFilename=file.getName();//Getting File Names into a variable
				Log.message("Actual File name is:"+ActualFilename);
				SkySiteUtils.waitTill(1000);	
				if(ActualFilename.contains("Documents"))						
				{
					Log.message("Downloaded csv file is available in downloads!!!");							
					break;
				}
							
			}
						
		}
		
		//Validating the CSV file from download folder	
		String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
		BufferedReader br = null;
		String line = null; 
		String splitBy = ",";
		int count = 0;
		String ActProjName = null;
		String ActProjNum = null;
		String ActDescription = null;
		String ActSiteAddress = null;
		String ActCity = null;
		String ActZip = null;
		String ActCountry = null;
		String ActState = null;
		String ActPasswordStatus = null;
		String ActDate = null;
		int Match_Count = 0;
		
		br = new BufferedReader(new FileReader(csvFileToRead)); 
		while ((line = br.readLine()) != null) 
		{  
			count=count+1;
			String[] ActValue = line.split(splitBy);
			ActProjNum = ActValue[0].replace("\"", "");
			//Log.message("Project Number from csv file is: "+ ActProjNum);
			
			ActProjName = ActValue[1].replace("\"", "");
			//Log.message("Project Name from csv file is: "+ ActProjName);
			
			ActSiteAddress = ActValue[2].replace("\"", "");
			//Log.message("Project Site Address from csv file is: "+ ActSiteAddress);
						
			ActCity = ActValue[3].replace("\"", "");
			//Log.message("Project City from csv file is: "+ ActCity);
						
			ActZip = ActValue[4].replace("\"", "");
			//Log.message("Project Zip from csv file is: "+ ActZip);
						
			ActDescription = ActValue[5].replace("\"", "");
			//Log.message("Project Description from csv file is: "+ ActDescription);
						
			ActState = ActValue[6].replace("\"", "");
			//Log.message("Project State from csv file is: "+ ActState);
			
			ActCountry = ActValue[7].replace("\"", "");
			//Log.message("Project Country from csv file is: "+ ActCountry);
			
			ActDate = ActValue[9].replace("\"", "");
			//Log.message("Project Start Date from csv file is: "+ ActDate);
			
			ActPasswordStatus = ActValue[10].replace("\"", "");
			//Log.message("Project Password Status from csv file is: "+ ActPasswordStatus);
			
			
			//Validate the download csv file
			if((ActProjNum.contentEquals(Edit_projectNumber))&&(ActProjName.contentEquals(Edit_projectName))&&(ActSiteAddress.contentEquals(Edit_SiteAddress))
					&&(ActCity.contentEquals(Edit_City))&&(ActZip.contentEquals(Edit_Zip))&&(ActDescription.contentEquals(Edit_Description))
					&&(ActState.contentEquals(Edit_state))&&(ActCountry.contentEquals(Edit_Country))&&(ActPasswordStatus.equalsIgnoreCase("True"))
					&&(ActDate.contentEquals(Edit_Date)))
			{
				Log.message("All Modified Project details are available in Exported csv File!!!");
				Match_Count=Match_Count+1;
			}
			
		}
						
		if(Match_Count==1)
		{
			return true;
		}
		else
		{
			Log.message("All Modified Project details are NOT available in Exported csv File!!!");
			return false;
		}
	
	}
	
	
	/** 
	 * Method written for validating Search a project and Export to csv.
	 * Scripted By : NARESH BABU KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	public boolean Validate_SearchAProject_Export(String csvFileToRead) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		//PrivateProjectsTab.click();
		WebElement EditLink = PrivateProjectsTab;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
		Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
	//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		boolean Results = false;
		if(Avl_Projects_Count>=1)
		{
			String First_Prj_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li[1]/div/section[1]/h4/span")).getText();
			txtGlobalSearch.sendKeys(First_Prj_Name);//Search with 1st Project
			bunSearch.click();//Click on search button
			SkySiteUtils.waitTill(10000);
			Results = this.Validate_ProjectExport(csvFileToRead);//Calling Export Validation Method
		}
		
		if(Results==true)
		{
			Log.message("Export a Project after search and validate is success!!!");
			return true;
		}
		else
		{
			Log.message("Export a Project after search and validate is FAILED!!!");
			return false;
		}
	}

	
	/** 
	 * Method written for selecting a project.
	 * Scripted By : NARESH
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	

	
	/** 
	 * Method written for selecting a Contacts button from top of the Dash board page.
	 * Scripted By : NARESH
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	/*public ContactsPage Validate_SelectContactsBut() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnContacts, 30);
		Log.message("Waiting for Contacts button to be appeared on top of Dashboard");
		btnContacts.click();
		Log.message("Clicked on Contacts button");
		SkySiteUtils.waitForElement(driver, btnAddNewContact, 20);
		SkySiteUtils.waitTill(3000);
		
		return new ContactsPage(driver).get();
	}
	*/
	
}
