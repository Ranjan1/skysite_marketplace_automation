package com.arc.SkysiteMarketPlace.testscripts;

import java.io.File;
import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.marketPlace.pages.ProjectDashboardPage;
import com.arc.marketPlace.pages.RegistrationPage;
import com.arc.marketPlace.utils.Generate_Random_Number;
import com.arc.marketPlace.utils.PropertyReader;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.*;


@Listeners(EmailReport.class)
public class RegistrationPageTestCases {
	 

		static WebDriver driver;	    
		RegistrationPage registrationPage;
	    ProjectDashboardPage projectDashboardPage;   
	   	Generate_Random_Number generate_Random_Number;	   	    
	   	   
	   public static String FullName = PropertyReader.getProperty("fullName");
	   public static String state = PropertyReader.getProperty("state");
	   public static String state2 = PropertyReader.getProperty("state1");
	   public static String phoneNumber = PropertyReader.getProperty("phoneNumber");
	   public static String password = PropertyReader.getProperty("password");
	  
	    
	    
	    @Parameters("browser")
	    @BeforeMethod
	 public WebDriver beforeTest(String browser) {
	        
	        if(browser.equalsIgnoreCase("firefox")) {
	               File dest = new File("./drivers/win/geckodriver.exe");
	               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	               driver = new FirefoxDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL_market"));
	 }
	        else if (browser.equalsIgnoreCase("chrome")) 
	        { 
	        File dest = new File("./drivers/win/chromedriver.exe");
	        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--start-maximized");
	        driver = new ChromeDriver( options );
	        driver.get(PropertyReader.getProperty("SkysiteProdURL_market"));
	 
	   } 
	        else if (browser.equalsIgnoreCase("safari"))
	        {
	               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
	               driver = new SafariDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL_market"));
	        }
	   return driver;
	 }
	     
		
	
	
	
	
     /** TC_1 (Registration Page):Verification Of Registration Page Creation . 
      *  Scripted By Ranjan P
      *  
     * @throws Exception
     */
     @Test(priority = 0, enabled = true, description = "Verification Of Registration Page creation ")
     public void Verify_RegistrationNumber() throws Exception
     {
    	 
     	try
     	{
     		
     		
     		Log.testCaseInfo("TC_1(Registration Page Creation): Verification Registration Page Creation ");
     	     //========Login Entry Into The Application=================================   
     		//Log.message("1234");
     		registrationPage = new RegistrationPage(driver).get(); 
     		//Log.message("123");
     		//registrationPage.FreeTrailLink();      		
     		Log.assertThat(registrationPage.FreeTrailLink(), "Free Trial Link  Successfully", "Free Trial Link Not Working", driver);
     		//registrationPage.RegistrationPage(FullName,state,phoneNumber,password);
     		Log.assertThat(registrationPage.RegistrationPage(FullName,state,phoneNumber,password), "Process Started", "Process Stopped", driver);
     		registrationPage.LogOut();
     			
     		
     	}
         catch(Exception e)
         {
         	e.getCause();
         	Log.exception(e, driver);
         }
     	finally
     	{
     		//Log.endTestCase();
     		driver.close();
     	}
     	
     }
     	/** TC_2 (Registration Page):Verification Of Registration Page Creation with International. 
         *  Scripted By Ranjan P
         *  
        * @throws Exception
        */
        @Test(priority = 1, enabled = true, description = "Verification Of Registration Page creation with International")
        public void Verify_RegistrationNumber_OtherLocation() throws Exception
        {
       	 
        	try
        	{
        		
        		
        		Log.testCaseInfo("TC_2(Registration Page Creation): Verification Registration Page Creation with International ");
        	    //========Login Entry Into The Application=================================     		
        		registrationPage = new RegistrationPage(driver).get();  
        		//registrationPage.FreeTrailLink();        		
        		Log.assertThat(registrationPage.FreeTrailLink(), "Free Trial Link Working Successfully", "Free Trial Link Not Working sucessfully", driver);		
        		//registrationPage.RegistrationPage(FullName,state2,phoneNumber,password);
        		Log.assertThat(registrationPage.RegistrationPage(FullName,state2,phoneNumber,password), "Process Started", "Process Stopped", driver);
        		registrationPage.LogOut();
         	 		
        		
        	}
            catch(Exception e)
            {
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		//Log.endTestCase();
        		driver.quit();
        	}
     }
      
     
     
   
}
